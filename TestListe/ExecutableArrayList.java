package TestListe;

import java.util.ArrayList;
import java.util.List;

class ExecutableArrayList {

    public static void main(String[] args) {
    
        List<Integer> listeEntier = new ArrayList<>();
        List<Integer> listeEntier2 = new ArrayList<>();
        System.out.println("Ici il y a : "+listeEntier);
        listeEntier.add(35);
        listeEntier.add(12);
        listeEntier.add(332);
        listeEntier.add(45);
        listeEntier.add(5);
        listeEntier.add(45);
        System.out.println("Mais ici il y a : " + listeEntier);
        listeEntier2.add(12);
        listeEntier2.add(48);
        listeEntier2.add(62);
        listeEntier2.add(156);
        listeEntier2.add(78);
        listeEntier2.addAll(listeEntier);
        System.out.println("Sur cette deuxieme liste nous avons : " + listeEntier2);
        System.out.println("Ici on voit les entiers de l'index i (inclue) à j (exclu) : " + listeEntier2.subList(0, 3));
        System.out.println("Ici on voit les entiers de l'index i à j qui est superieur à la taille de la liste qui est : " + listeEntier2.size() + " le code erreur est : java.lang.IndexOutOfBoundsException: toIndex = 45 ");
    }

}