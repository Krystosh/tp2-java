package LesMagasins;

import java.util.ArrayList ;
import java.util.List ;

public class ListeMagasins {
   private List<Magasin> magasins;

   public ListeMagasins() {
      this.magasins = new ArrayList<>();
   }

   public void ajoute(Magasin mag) {
      this.magasins.add(mag);
   }
   @Override
   public String toString() {return this.magasins.toString();}

   public List<Magasin> ouvertsLeLundi() {
      List<Magasin> res = new ArrayList<>();
      for (Magasin m : this.magasins) {
         if (m.getLundi()) {
            res.add(m);
         }
      }
      return res;
   }
}