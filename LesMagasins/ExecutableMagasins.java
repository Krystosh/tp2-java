package LesMagasins;


public class ExecutableMagasins {

    public static void main(String[] args) {
        
        ListeMagasins liste1 = new ListeMagasins();
        Magasin fleurus = new Magasin("Fleurus", true, false);
        Magasin beauMagasin = new Magasin("BeauMagasin", true, true);
        Magasin venir = new Magasin("Venir", false, false);
        Magasin magnifique = new Magasin("Magnifique", false, true);

        liste1.ajoute(fleurus);
        liste1.ajoute(beauMagasin);
        liste1.ajoute(venir);
        liste1.ajoute(magnifique);

        System.out.println(liste1);
        System.out.println("Voici les magasins ouver le lundi : " + liste1.ouvertsLeLundi());

    }
    
}
