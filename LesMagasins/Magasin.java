package LesMagasins;

public class Magasin {
    private String nom;
    private boolean ouvertLundi, ouvertDimanche ;
 
    public Magasin(String nom, boolean lundi, boolean dimanche) {
        this.nom = nom;
        this.ouvertLundi = lundi;
        this.ouvertDimanche = dimanche;
    }

    // Les getters

    public String getNom() {return this.nom;} 
    public boolean getLundi(){return this.ouvertLundi;}
    public boolean getDimache(){return this.ouvertDimanche;}

 
    @Override
    public String toString() {
        String res = "Le magasin " + this.nom;
        if (this.ouvertLundi) {
            res+= " est ouvert le lundi";
        }
        else {
            res+= " est fermé le lundi";
        }
        if (this.ouvertDimanche) {
            res+= " et ouvert le dimanche";
        }
        else {
            res+=" et fermé le dimanche";
        }
        return res;
    }
 }