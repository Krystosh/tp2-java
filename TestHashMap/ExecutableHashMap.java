package TestHashMap;

import java.util.HashMap;

public class ExecutableHashMap {
    
    public static void main(String[] args) {
        HashMap<Integer,Integer> map =new HashMap<Integer, Integer>();
        map.put(1, 89);
        map.put(2, 78);
        map.put(3, 12);
        map.put(4, 7);
        map.put(5, 10);
        System.out.println(map);
        map.put(5,7);
        System.out.println(map);
        map.remove(3);
        System.out.println(map);
        map.clear();
        System.out.println(map);
    }
}
